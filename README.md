# Pong #

### What is this repository for? ###

* This is one of my earliest projects created around 2010 while I was in highschool. I then breifly modified it in 2016 with some minor improvements.
* Pong is the old classic where you have 2 paddles and the goal is to try and get the pong ball past the opponents paddle to score.

### How do I get set up? ###

* I left it in a rough state. 
* Currently it goes into a full screen mode where you have to press "R" to run it.

### TODO: ###

* Coming up: Will make immediate changes to ensure this project can run on all computers without an incomplete start menu and without a full screen mode
* Future Updates:
* Need to add a Menu to change all of the game settings
* Need a massive overhall to this single file app
* Need to update to a newer framework
