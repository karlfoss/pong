﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

/**
 * This main form is the entire application of the game of pong. It doesn't make sense to make multiple forms for the same game. 
 * The goal of the game is to get the pong ball past the other player's paddle in order to score. Currently the game is being optimized for 1920x1080p. 
 *          -consider making another class for the start menu
 *          -or just making a menu that pops up over everything else
 *          
 * BUGS:
 *  1 - Ball doesn't appear normally and will 'blip' a lot. Fix it's timing and maybe use image to load it instead
 *  2 - the ball curve stays on after changing to the no curve setting
 *  
 *  
 * ENHANCEMENTS
 *  1 - Make a game menu to change the settings of the game (ie resolution, ball, paddle speed), then make button to start game and use a button press to leave game
 *  2 - Update the game to work at whatever resolution the user 'drags' the form1 box to be
 *  3 - Enable a 'debug' mode where it shows all of the current game settings the way it currently does now
 *  4 - Make it possible to change backgrounds, paddles, balls etc by uploading images for skins in the game settings
 *  5 - Divide this class into multiple classes. Probably need another one for the start screen...game settings (make into multiples classes, instances, objects, getters setters...etc)
 *      - Add scoreboard to these margins (maybe debug settings as well if they are also on
 *  7 - Base the ball curve on the distance from the center of the paddle (ie center = no curve, edge = mega curve)
 *  8 - Remove all hardcoded values and repleace with constants. There should be no numbers
 *      - Maybe add them to a properties file to clean up the code (think of the files that I read to change video game settings)
 *  9 - With each major setting (easy, medium, hard) - make the game winable
 *      - Furthermore, hard = smaller paddle size and tougher ai, easy = larger paddle size and easier ai
 *      - Paddle speed must correlate with paddle size for effectiveness (normal paddle = normal speed)
 *  10 - Make it so cheats can be enabled and disabled
 *  11 - Ball will pass through paddle. Either make ball bounce of bottom or top if it comes from below or above. Otherwise make paddle more narrow
 *      - also consider making the ball bounce off without having to have a perfect hit...say maybe only 75% to 50%, this would reduce passthrough
 *  12 - Make it possible to turn debug settings on and off on the fly
 *  13 - Have the ball curve a little bit on the fly as well to make it less predictable (by changing acceleration on both x and y axis)
 *  14 - Add a button to quit the game at anytime
 *  15 - If there is less of a curve...make the ball go faster...(in order to match it's speed on the hardest curve possible)...thus keeping the ball speed consistant and making it easier to score
 *  16 - The ai's right paddle will glitch because it isn't even with the metric that moves it up and down
 *  17 - Offer different game modes...one can be crazy ball (see #13), offer another mode where the ball gets faster and faster after each hit
 *  18 - remove all integers and make them into the doubles to stop the paddles from glitching when the ball is moving (ie the paddle position won't round different each time when calculating the contact point)
 *  19 - Have the paddles always start in the in the exact middle before each serve and make sure that the ball goes straight to the person is down
 *  20 - Allow users to change the keys that move them up or down and prevent conflicts (ie multiple key mappings)
 *  21 - Hide the start menu when game is playing, high the game visuals when the start menu is displayed
 *  
 */
namespace pong
{
    public partial class Form1 : Form
    {
        //TODO: make all the names better and add contants for all of the set values
        public int s = 1;
        public int z = 50;
        public int z1 = 0;
        // pong ball's x axis speed and direction, if greater than 0 the ball is going downward
        public int ballXAxisVelocity = 5;
        // pong ball's y axis speed and direction, if greater than 0 the ball is going right
        public int ballYAxisVelocity = 5;
        // The left paddle's movement speed
        public int leftPaddleMovementSpeed = 50;
        // The left paddle's position on the x axis
        public int leftPaddleXPosition;
        // The left paddle's position on the y axis
        public int leftPaddleYPosition;
        // The right paddle's position on the x axis
        public int rightPaddleXPosition;
        // The right paddle's position on the y axis
        public int rightPaddleYPosition;
        // The ball's position on the x axis
        public int ballXPosition;
        // The ball's position on the y axis
        public int ballYPosition;
        // The score of the player on the left side of the screen (ie player one)
        public int leftPlayerScore = 0;
        // The score of the player on the right side of the screen (ie player two)
        public int rightPlayerScore = 0;
        // This is the y coordinate at  which the ball will contact the right paddle's left border
        // Used by the ai to predict the balls movement on hard difficulty
        public int rightPaddleContactPoint = 0;
        // TODO: determine all of the settings for the user to set
        public ComboBox gameType = new ComboBox();
        // The button to start the game (TODO: not working)
        public PictureBox startButton = new PictureBox();
        // The screen of the game before it starts. Can change settings here (TODO: not working)
        public PictureBox startMenu = new PictureBox();
        // The paddle on the left side of the game screen
        public PictureBox leftPaddle = new PictureBox();
        // The paddle on the right side of the game screen
        public PictureBox rightPaddle = new PictureBox();
        // The pong ball
        public PictureBox ball = new PictureBox();
        // The scorebox of the player on the left
        public Label leftPlayerScoreBox = new Label();
        // The scorebox of the player on the right
        public Label rightPlayerScoreBox = new Label();
        public Label p3s = new Label();
        public Label p4s = new Label();
        public Label p5s = new Label();
        public Label p6s = new Label();
        public Label p7s = new Label();
        public Label p8s = new Label();
        public Label p9s = new Label();
        public Label p10s = new Label();
        public bool ai = false;
        public bool force = false;
        // True while human is pressing down on 's' to move the left paddle down
        public bool leftPaddleMovingDown = false;
        // True while human is pressing down on 'w' to move the left paddle up
        public bool leftPaddleMovingUp = false;

        //Set to true to show menu on the fly and be able to change all settings at any point
        public bool debug = true;
        // The background of the debug panel to make it easier to see
        public PictureBox debugBackground = new PictureBox();
        Random rn = new Random();

        public int f = 1,fo=0, j = 1,b=1,g=1,g1=0,p=1,w=1,o=1;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Makes the game fullscreen
            //this.FormBorderStyle = FormBorderStyle.None;
            //this.WindowState = FormWindowState.Maximized;
            // This allows the windows.keyup events to fire off for the form
            KeyPreview = true;

            //TODO: add start menu
            //this.Controls.Add(startMenu);
            //this.Controls.Add(startButton);
            //this.Controls.Add(gameType);
            // Sets the width, height, and background of the game
            //this.Width = Screen.PrimaryScreen.Bounds.Width;
            //this.Height = Screen.PrimaryScreen.Bounds.Height;
            this.Width = 1920;
            this.Height = 1080;
            this.BackColor = Color.White;
            this.Controls.Add(leftPaddle);
            this.Controls.Add(rightPaddle);
            this.Controls.Add(ball);
            this.Controls.Add(leftPlayerScoreBox);
            this.Controls.Add(rightPlayerScoreBox);

            gameType.Items.Add("Easy");
            gameType.Items.Add("Medium");
            gameType.Items.Add("Hard");
            gameType.Height = 50;
            gameType.Width = 100;

            leftPaddle.Width = 25;
            leftPaddle.Height = 100;
            leftPaddle.Location = new System.Drawing.Point(leftPaddleXPosition = 50, leftPaddleYPosition = (this.Height / 2 - leftPaddle.Height / 2));
            leftPaddle.BackColor = Color.Green;

            rightPaddle.Width = 25;
            rightPaddle.Height = 100;
            rightPaddle.Location = new System.Drawing.Point(rightPaddleXPosition = this.Width - 75, rightPaddleYPosition = (this.Height / 2 - rightPaddle.Height / 2));
            rightPaddle.BackColor = Color.Green;

            ball.Location = new System.Drawing.Point(ballXPosition = (this.Width/2 - ball.Width/2), ballYPosition = (this.Height/2 - ball.Height/2));
//            ball.BackColor = Color.Blue;
            ball.Image = Image.FromFile("..\\..\\Image\\ballImage.png");
            ball.Height = 10;
            ball.Width = 10;

            leftPlayerScoreBox.Text = leftPlayerScore.ToString();
            leftPlayerScoreBox.Font = new Font(this.Font.FontFamily, 4f * this.Font.Size);
            leftPlayerScoreBox.ForeColor = Color.Black;
            leftPlayerScoreBox.Location = new System.Drawing.Point((this.Width - this.Width*3/4), this.Height/20);
            leftPlayerScoreBox.Height = 50;
            leftPlayerScoreBox.Width = 100;

            rightPlayerScoreBox.Text = rightPlayerScore.ToString();
            rightPlayerScoreBox.Font = new Font(this.Font.FontFamily, 4f * this.Font.Size);
            rightPlayerScoreBox.ForeColor = Color.Black;
            rightPlayerScoreBox.Location = new System.Drawing.Point((this.Width - this.Width/4), this.Height/20);
            rightPlayerScoreBox.Height = 50;
            rightPlayerScoreBox.Width = 100;

            startMenu.Text = "Welcome to Pong!\nPress 'S' to start the game!";
            startMenu.ForeColor = Color.Black;
            startMenu.Location = new System.Drawing.Point(0, 0);
            startMenu.Height = this.Height;
            startMenu.Width = this.Width;
            startMenu.BackColor = Color.Green;
            startMenu.BringToFront();

            startButton.ForeColor = Color.White;
            startButton.Location = new System.Drawing.Point(100, 100);
            startButton.Text = "Welcome to Pong!\nPress 'S' to start the game!";
            startButton.Height = 200;
            startButton.Width = 200;
            startButton.BringToFront();
            startButton.BackColor = Color.Blue;
            
            ballm.Enabled = false;

            //If debugging is enabled allow use to change settings during game
            if (debug)
            {
                this.Controls.Add(p3s);
                this.Controls.Add(p4s);
                this.Controls.Add(p5s);
                this.Controls.Add(p6s);
                this.Controls.Add(p7s);
                this.Controls.Add(p8s);
                this.Controls.Add(p9s);
                this.Controls.Add(p10s);
                this.Controls.Add(debugBackground);

                p3s.Text = "F-Paddle Size = NORMAL";
                p3s.ForeColor = Color.Black;
                p3s.Location = new System.Drawing.Point(10, 649);
                p3s.Height = 25;
                p3s.Width = 200;
                p3s.BackColor = Color.Red;

                p4s.Text = "J-Paddle Size = NORMAL";
                p4s.ForeColor = Color.Black;
                p4s.Location = new System.Drawing.Point(700, 649);
                p4s.Height = 25;
                p4s.Width = 200;
                p4s.BackColor = Color.Red;

                p5s.Text = "A0 = PLAYER VS. PLAYER";
                p5s.ForeColor = Color.Black;
                p5s.Location = new System.Drawing.Point(210, 649);
                p5s.Height = 25;
                p5s.Width = 600;
                p5s.BackColor = Color.Red;

                p6s.Text = "B0 = NORMAL BALL";
                p6s.ForeColor = Color.Black;
                p6s.Location = new System.Drawing.Point(210, 674);
                p6s.Height = 25;
                p6s.Width = 350;
                p6s.BackColor = Color.Red;

                p7s.Text = "G0 = NO CURVE";
                p7s.ForeColor = Color.Black;
                p7s.Location = new System.Drawing.Point(210, 699);
                p7s.Height = 25;
                p7s.Width = 350;
                p7s.BackColor = Color.Red;

                p8s.Text = "P0 = BALL ON";
                p8s.ForeColor = Color.Black;
                p8s.Location = new System.Drawing.Point(210, 723);
                p8s.Height = 25;
                p8s.Width = 175;
                p8s.BackColor = Color.Red;

                p9s.Text = "Paddle Speed: Q0 = NORMAL";
                p9s.ForeColor = Color.Black;
                p9s.Location = new System.Drawing.Point(10, 723);
                p9s.Height = 25;
                p9s.Width = 300;
                p9s.BackColor = Color.Red;

                p10s.Text = "Paddle Speed: O0 = NORMAL";
                p10s.ForeColor = Color.Black;
                p10s.Location = new System.Drawing.Point(700, 723);
                p10s.Height = 25;
                p10s.Width = 275;
                p10s.BackColor = Color.Red;

                debugBackground.Width = 1000;
                debugBackground.Height = 200;
                debugBackground.Location = new System.Drawing.Point(0, 600);
                debugBackground.BackColor = Color.Red;
            }
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            // Start Game
            if (e.KeyCode == Keys.R)
            {
                //TODO: reset settings and scores too
                startMenu.Hide();
                startButton.Hide();
            }
            
            // Terminate Game
            if (e.KeyCode == Keys.T)
            {
                startMenu.Show();
                startButton.Show();
            }

            // Human player moves 
            if (e.KeyCode == Keys.W)
            {
                leftPaddleMovingUp = true;
            }
            if (e.KeyCode == Keys.S)
            {
                leftPaddleMovingDown = true;
            }
            if (o != 4)
            {
                if (e.KeyCode == Keys.Up)
                {
                    if (rightPaddle.Top >= 0)
                    {
                        rightPaddle.Top = rightPaddle.Top - z;
                    }
                    if (rightPaddle.Top < 0)
                    {
                        rightPaddle.Top = 0;
                    }
                }
                if (e.KeyCode == Keys.Down)
                {
                    if (rightPaddle.Bottom <= this.Height)
                    {
                        rightPaddle.Top = rightPaddle.Top + z;
                    }
                    if (rightPaddle.Bottom > this.Height)
                    {
                        rightPaddle.Top = rightPaddle.Top - (rightPaddle.Bottom - this.Height);
                    }
                }
            }
            //ball serve fix the rperson who reacies the ball ?angle doesn't change after serve?
            if (e.KeyCode == Keys.Enter && ballm.Enabled == false && leftPlayerScore == rightPlayerScore)
            {
                ballm.Enabled = true;
                ballYAxisVelocity = 0;
            }
            if (e.KeyCode == Keys.Enter && ballm.Enabled == false && leftPlayerScore > rightPlayerScore)
            {
                ballm.Enabled = true;
                ballYAxisVelocity = 0;
            }
            if (e.KeyCode == Keys.Enter && ballm.Enabled == false && leftPlayerScore < rightPlayerScore)
            {
                ballm.Enabled = true;
                ballYAxisVelocity = 0;
                ballXAxisVelocity = -ballXAxisVelocity;
            }
            //cheats
            //  create mulitple distracions
            if (e.KeyCode == Keys.C)
            {
                ballXAxisVelocity = -ballXAxisVelocity;
            }
            if (e.KeyCode == Keys.X)
            {
                ballXAxisVelocity = 50;
            }
            if (e.KeyCode == Keys.V)
            {
                fo = fo + 1;
                if (fo == 1) force = true;
                if (fo == 2)
                {
                    fo = 0;
                    force = false;
                }
            }
            //settings make increase ball mode + score clear + auto move
            if (e.KeyCode == Keys.F && debug)
            {
                f = f + 1;
                if (f == 1) 
                {
                    leftPaddle.Height = 100;
                    p3s.Text = "F-Paddle Size = NORMAL";
                }
                if (f == 2)
                {
                    leftPaddle.Height = 150;
                    p3s.Text = "F-Paddle Size = LARGE";
                }
                if (f == 3)
                {
                    leftPaddle.Height = 200;
                    p3s.Text = "F-Paddle Size = XL";
                    f = 0;
                }
            }
            if (e.KeyCode == Keys.J && debug)
            {
                j = j + 1;
                if (j == 1)
                {
                    rightPaddle.Height = 100;
                    p4s.Text = "J-Paddle Size = NORMAL";
                }
                if (j == 2)
                {
                    rightPaddle.Height = 150;
                    p4s.Text = "J-Paddle Size = LARGE ";
                }
                if (j == 3)
                {
                    rightPaddle.Height = 200;
                    p4s.Text = "J-Paddle Size = XL";
                    j = 0;
                }
            }
            if (e.KeyCode == Keys.A && debug)
            {
                s = s + 1;
                if (s == 1)
                {
                    p5s.Text = "A0 = PLAYER VS. PLAYER A1";
                    z = 50;
                    if (j == 1 || j == 0) rightPaddle.Height = 100;
                    if (j == 2) rightPaddle.Height = 150;
                    if (j == 3) rightPaddle.Height = 200;

                }
                if (s == 2)
                {
                    ai = true;
                    p5s.Text = "A1 = PLAYER VS. EASY PC";
                }

                if (s == 3)p5s.Text = "A2 = PLAYER VS. MEDIUM PC";
                
                if (s == 4) p5s.Text = "A3 = PLAYER VS. HARD PC";
                if (s == 5)
                {
                    ai = false;
                    rightPaddle.Top = 0;
                    rightPaddle.Height = this.Height;
                    p5s.Text = "A4 = PLAYER VS. SELF";
                    s = 0;
                }
            }
            if (e.KeyCode == Keys.B && debug)
            {
                b = b + 1;
                if (b == 1)
                {
                    p6s.Text = "B0 = NORMAL BALL";
                    ballXAxisVelocity = 4;
                }
                if (b == 2)
                {
                    p6s.Text = "B1 = FAST BALL";
                    ballXAxisVelocity = 8;
                }
                if (b == 3)
                {
                    p6s.Text = "B2 = VERY FAST BALL";
                    ballXAxisVelocity = 12;
                }
                if (b == 4)
                {
                    p6s.Text = "B3 = SUPER BALL";
                    b = 0;
                    ballXAxisVelocity = 16;
                }
            }
            if (e.KeyCode == Keys.G && debug)
            {
                g = g + 1;
                if (g == 1)
                {
                    p7s.Text = "G0 = NO CURVE";
                    g1 = 1;
                }
                if (g == 2)
                {
                    p7s.Text = "G1 = SMALL CURVE";
                    g1 = 2;
                }
                if (g == 3)
                {
                    p7s.Text = "G2 = LARGE CURVE";
                    g1 = 3;
                }
                if (g == 4)
                {
                    p7s.Text = "G3 = HUGE CURVE";
                    g1 = 4;
                    g = 0;
                }
            }
            if (e.KeyCode == Keys.P)
            {
                p = p + 1;
                if (p == 1)
                {
                    ballm.Enabled = true;
                    p8s.Text = "P0 = BALL ON";
                }
                if (p == 2)
                {
                    ballm.Enabled = false;
                    p8s.Text = "P1 = BALL PAUSED";
                    p = 0;
                }
            }
            if (e.KeyCode == Keys.O && debug)
            {
                o = o + 1;
                if (o == 1)
                {
                    p10s.Text = "O0 = NORMAL";
                    z = 50;
                }
                if (o == 2)
                {
                    p10s.Text = "O1 = FAST";
                    z = 75;
                }
                if (o == 3)
                {
                    p10s.Text = "O2 = VERY FAST";
                    z = 100;
                }
                if (o == 4)
                {
                    o = 0;
                    z = 0;
                }
            }
            if (e.KeyCode == Keys.Q && debug)
            {
                w = w + 1;
                if (w == 1)
                {
                    p9s.Text = "Q0 = NORMAL";
                    leftPaddleMovementSpeed = 50;
                }
                if (w == 2)
                {
                    p9s.Text = "Q1 = FAST ";
                    leftPaddleMovementSpeed = 75;
                }
                if (w == 3)
                {
                    p9s.Text = "Q2 = VERY FAST";
                    w = 0;
                    leftPaddleMovementSpeed = 100;
                }
            }
        }


        protected override void OnKeyUp(KeyEventArgs e)
        {
            // Human player stops moving left paddle up
            if (e.KeyCode == Keys.W)
            {
                leftPaddleMovingUp = false;
            }
            // Human player stops moving left paddle down
            if (e.KeyCode == Keys.S)
            {
                leftPaddleMovingDown = false;
            }
        }


        private void ballm_Tick(object sender, EventArgs e)
        {
            // Move the left paddle down if human is pressing 's' key
            if (leftPaddleMovingDown)
            {
                if (leftPaddle.Bottom < this.Height)
                {
                    leftPaddle.Top = leftPaddle.Top + 10;
                }
            }
            // Move the left paddle up if human is pressing 'W' key
            if (leftPaddleMovingUp)
            {
                if (leftPaddle.Top > 0)
                {
                    leftPaddle.Top = leftPaddle.Top - 10;
                }
            }

            // Move the left paddle 
            //forcefield
            if (force == true && ball.Left <= leftPaddle.Right)
            {
                //Fix so ball doesn't get stuck in force field
                if (ballXAxisVelocity < 0)
                {
                    ballXAxisVelocity = -ballXAxisVelocity;
                }
                if (g1 == 1)
                {
                    if (ballYAxisVelocity >= 0) ballYAxisVelocity = 5;
                    if (ballYAxisVelocity < 0) ballYAxisVelocity = -5;
                }
                if (g1 == 2) ballYAxisVelocity = rn.Next(-5, 6);
                if (g1 == 3) ballYAxisVelocity = rn.Next(10, 11);
                if (g1 == 4) ballYAxisVelocity = rn.Next(-15, 16);
            }
            //ai shorter code for hard + make paddle move depeding on its height+keep 
            //ai from moveing out of bounds+ make ai vs ai instead of player vs. self with different ais
            //TODO: make the below code recurive to a certain depth before it stops
            if (ai == true)
            {
                z = 0;

                if (s == 2)
                {
                    if (ball.Top < rightPaddle.Top + rightPaddle.Height / 2)
                    {
                        // NOTE: The 6 is equal to the speed of the paddle
                        rightPaddle.Top = rightPaddle.Top - 6;
                    }
                    if (ball.Bottom > rightPaddle.Bottom - rightPaddle.Height / 2)
                    {
                        // NOTE: The 6 is equal to the speed of the paddle
                        rightPaddle.Top = rightPaddle.Top + 6;
                    }
                }
                if (s == 3)
                {
                    if (ballXAxisVelocity > 0)
                    {
                        if (ball.Top < rightPaddle.Top+50)
                        {
                            // NOTE: The 9 is equal to the speed of the paddle
                            rightPaddle.Top = rightPaddle.Top - 9;
                        }
                        if (ball.Bottom > rightPaddle.Bottom-50)
                        {
                            // NOTE: The 9 is equal to the speed of the paddle
                            rightPaddle.Top = rightPaddle.Top + 9;
                        }
                    }
                    if (ballXAxisVelocity < 0)
                    {
                        // NOTE: The 5 is equal to the speed of the paddle returning to the base position
                        if (rightPaddle.Top < (this.Height / 2 - rightPaddle.Height / 2)) rightPaddle.Top = rightPaddle.Top + 5;
                        if (rightPaddle.Top > (this.Height / 2 - rightPaddle.Height / 2)) rightPaddle.Top = rightPaddle.Top - 5;
                    }
                }
                if (s == 4)
                {
                    if (ballXAxisVelocity > 0)
                    {
                        determinteRightContactPoint();
                    }
                    if (ballXAxisVelocity > 0)
                    {
                        // NOTE: The 5 is equal to the speed of the paddle
                        if (rightPaddle.Top + rightPaddle.Height/2 < rightPaddleContactPoint) rightPaddle.Top = rightPaddle.Top + 5;
                        if (rightPaddle.Top + rightPaddle.Height/2 > rightPaddleContactPoint) rightPaddle.Top = rightPaddle.Top - 5;
                    }
                    if (ballXAxisVelocity < 0)
                    {
                        // NOTE: The 5 is equal to the speed of the paddle returning to the base position
                        if (rightPaddle.Top < (this.Height/2 - rightPaddle.Height/2)) rightPaddle.Top = rightPaddle.Top + 5;
                        if (rightPaddle.Top > (this.Height/2 - rightPaddle.Height/2)) rightPaddle.Top = rightPaddle.Top - 5;
                    }
                }
            }
            //ball movement
            if (ball.Bottom >= this.Height && ballYAxisVelocity > 0)
            {
                ballYAxisVelocity = -ballYAxisVelocity;
            }
            //TODO: fix may be needed here. This code means all of the ball has to hit the paddle to 'register' the hit, may want only partial hit (0-99%)?
            if (rightPaddle.Left <= ball.Right && rightPaddle.Right > ball.Right && ball.Bottom <= rightPaddle.Bottom && ball.Top >= rightPaddle.Top)
            {

                //Fix so ball doesn't get stuck in force field
                if (ballXAxisVelocity > 0)
                {
                    ballXAxisVelocity = -ballXAxisVelocity;
                }
                if (g1 == 1)
                {
                    if (ballYAxisVelocity >= 0) ballYAxisVelocity = 5;
                    if (ballYAxisVelocity < 0) ballYAxisVelocity = -5;
                }
                if (g1 == 2) ballYAxisVelocity = rn.Next(-5, 6);
                if (g1 == 3) ballYAxisVelocity = rn.Next(-10, 11);
                if (g1 == 4) ballYAxisVelocity = rn.Next(-15, 16);
                //fix to cheat speed
                if (ballXAxisVelocity == 50 || ballXAxisVelocity == -50)
                {
                    if (b == 4) ballXAxisVelocity = -16;
                    if (b == 3) ballXAxisVelocity = -12;
                    if (b == 2) ballXAxisVelocity = -8;
                    if (b == 1) ballXAxisVelocity = -4;
                }

            }
            if (ball.Top <= 0 && ballYAxisVelocity < 0)
            {
                ballYAxisVelocity = -ballYAxisVelocity;
            }
            if (force == false)
            {
                //TODO: fix may be needed here. This code lets means all of the ball has to hit the paddle to 'register' the hit, may want only partial hit (0-99%)?
                if (leftPaddle.Right >= ball.Left && leftPaddle.Left < ball.Left && ball.Bottom <= leftPaddle.Bottom && ball.Top >= leftPaddle.Top)
                {
                    //Fix so ball doesn't get stuck in force field
                    if (ballXAxisVelocity < 0)
                    {
                        ballXAxisVelocity = -ballXAxisVelocity;
                    }
                    if (g1 == 1)
                    {
                        if (ballYAxisVelocity >= 0) ballYAxisVelocity = 5;
                        if (ballYAxisVelocity < 0) ballYAxisVelocity = -5;
                    }
                    if (g1 == 2) ballYAxisVelocity = rn.Next(-5, 6);
                    if (g1 == 3) ballYAxisVelocity = rn.Next(10, 11);
                    if (g1 == 4) ballYAxisVelocity = rn.Next(-15, 16);
                }
            }
            // Player 2 scores
            if (ball.Left <= 0)
            {
                ballm.Enabled = false;
                ball.Location = new System.Drawing.Point(ballXPosition = (this.Width / 2 - ball.Width / 2), ballYPosition = (this.Height / 2 - ball.Height / 2));
                rightPlayerScore = rightPlayerScore + 1;
                rightPlayerScoreBox.Text = rightPlayerScore.ToString();

            }
            // Player 1 scores
            if (ball.Right >= this.Width)
            {
                ballm.Enabled = false;
                ball.Location = new System.Drawing.Point(ballXPosition = (this.Width / 2 - ball.Width / 2), ballYPosition = (this.Height / 2 - ball.Height / 2));
                leftPlayerScore = leftPlayerScore + 1;
                leftPlayerScoreBox.Text = leftPlayerScore.ToString();
                //fix to cheat speed
                if (ballXAxisVelocity == 50 || ballXAxisVelocity == -50)
                {
                    if (b == 4) ballXAxisVelocity = -16;
                    if (b == 3) ballXAxisVelocity = -12;
                    if (b == 2) ballXAxisVelocity = -8;
                    if (b == 1) ballXAxisVelocity = -4;
                }
            }
            ball.Left = ball.Left + ballXAxisVelocity;
            ball.Top = ball.Top + ballYAxisVelocity;   
        }


        //////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////Private Helper Methods//////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////

        
        /*
         * Determines the point at which the ball will pass the right paddle's left border
         * 
         * returns the y axis point
         */
        private void determinteRightContactPoint()
        {
            // Gets the number of 'ticks' the ball is away from the right paddle to get the y axis point at which it will reach the left border of the right paddle
            rightPaddleContactPoint = ((rightPaddle.Left - ball.Right) / ballXAxisVelocity) * ballYAxisVelocity + ball.Top - ball.Height/2;
            rightPaddleContactPoint = determinteRightContactPoint(rightPaddleContactPoint);
        }

        /**
         * Helper method for determining the the point at which the ball will pass the right paddle's left border
         */
        private int determinteRightContactPoint(int rightPaddleContactPointTemp)
        {
            // If the ball will hit the top of the gamescreen, adjust the contact point for the Ricochet
            if (rightPaddleContactPointTemp < 0)
            {
                rightPaddleContactPointTemp = -rightPaddleContactPointTemp;
                return determinteRightContactPoint(rightPaddleContactPointTemp);
            }
            // If the ball will hit the bottom of the gamescreen, adjust the contact point for the Ricochet
            if (rightPaddleContactPointTemp > this.Height)
            {
                rightPaddleContactPointTemp = rightPaddleContactPointTemp - this.Height;
                rightPaddleContactPointTemp = this.Height - rightPaddleContactPointTemp;
                return determinteRightContactPoint(rightPaddleContactPointTemp);
            }
            // The ball is within the gamescreen, no more adjustment is needed
            return rightPaddleContactPointTemp;
        }
    }
}